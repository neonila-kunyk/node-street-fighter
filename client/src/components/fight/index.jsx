import React from "react";

import { getFighters } from "../../services/domainRequest/fightersRequest";
import NewFighter from "../newFighter";
import Fighter from "../fighter";
import { Button } from "@material-ui/core";
import HealthBar from '../health-bar';
import FighterView from '../fighter-view';
import { controls } from '../../constants/controls';
import { FIGHT_INTERVAL, CRITIKAL_HIT_INTERVAL } from '../../constants/times';

import "./fight.css";

import { createFight } from "../../services/domainRequest/fightRequest";

class Fight extends React.Component {
  state = {
    fighters: [],
    fighter1: null,
    fighter2: null,
    winner: null,
    width1: "100%",
    width2: "100%",
    isGameStarted: false,
    log: []
  };

  async componentDidMount() {
    const fighters = await getFighters();
    if (fighters && !fighters.error) {
      this.setState({ fighters });
    }
  }

  onFightStart = async () => {
    const { fighter1, fighter2 } = this.state;

    if (fighter1 && fighter2) {
      this.setState({
        isGameStarted: true,
      });
      const winner = await this.fight();
      this.setState({ winner });
    } else {
      this.setState({
        isGameStarted: false,
      });
    }
  };

  onFightEnd = async () => {
    const { fighter1, fighter2, log } = this.state;
    await createFight({
        fighter1,
        fighter2,
        log
    });
    this.setState({ 
        fighter1: null,
        fighter2: null,
        isGameStarted: false, 
        winner: null,  
        width1: "100%",
        width2: "100%",
        log: []
    });
  };

  onCreate = (fighter) => {
    this.setState({ fighters: [...this.state.fighters, fighter] });
  };

  onFighter1Select = (fighter1) => {
    this.setState({ fighter1 });
  };

  onFighter2Select = (fighter2) => {
    this.setState({ fighter2 });
  };

  getFighter1List = () => {
    const { fighter2, fighters } = this.state;
    if (!fighter2) {
      return fighters;
    }

    return fighters.filter((it) => it.id !== fighter2.id);
  };

  getFighter2List = () => {
    const { fighter1, fighters } = this.state;
    if (!fighter1) {
      return fighters;
    }

    return fighters.filter((it) => it.id !== fighter1.id);
  };

  async fight() {
    const { fighter1, fighter2 } = this.state;
    const leftFighter = this.initFighter(fighter1, 'left');
    const rightFighter = this.initFighter(fighter2, 'right');
    
    const onKeyDown = (event) => {
        this.keyDownHandler(event, leftFighter, rightFighter);
    }
    
    const onKeyUp = (event) => {
        this.keyUpHandler(event, leftFighter, rightFighter);
    }
    
    document.addEventListener('keydown', onKeyDown, false);
    document.addEventListener('keyup', onKeyUp, false);

    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over
        const timer = setInterval(()=> {
        if (leftFighter.currentHealth === 0 || rightFighter.currentHealth === 0) {
            document.removeEventListener('keydown', onKeyDown, false);
            document.removeEventListener('keyup', onKeyUp, false);
            clearInterval(timer);
            const winner = leftFighter.currentHealth === 0 ? rightFighter : leftFighter;
            resolve(winner);
        }
        }, FIGHT_INTERVAL)
    });
  }

  getDamage(attacker, defender) {
    // return damage
    const damage = this.getHitPower(attacker) - this.getBlockPower(defender);
    return Math.max(0, damage);
  }

  getHitPower(fighter) {
    // return hit power
    const criticalHitChance = this.getRandom(1, 2);
    return fighter.power * criticalHitChance;
  }

  getBlockPower(fighter) {
    // return block power
    const dodgeChance = this.getRandom(1, 2);
    return fighter.defense * dodgeChance;
  }

  getRandom(min, max) {
    return Math.random() * (max - min) + min;
  }

  keyDownHandler(event, leftFighter, rightFighter) {
    switch(event.code){
        case controls.PlayerOneAttack: {
            this.attack(leftFighter, rightFighter);
            break;
        }
        case controls.PlayerOneBlock: {
            this.blockOn(leftFighter);
            break;
        }
        case controls.PlayerOneCriticalHitCombination[0]: {
            this.criticalHitKeyDownHandler (event.code, leftFighter, rightFighter);
            break;
        }
        case controls.PlayerOneCriticalHitCombination[1]: {
            this.criticalHitKeyDownHandler (event.code, leftFighter, rightFighter);
            break;
        }
        case controls.PlayerOneCriticalHitCombination[2]: {
            this.criticalHitKeyDownHandler (event.code, leftFighter, rightFighter);
            break;
        }
        case controls.PlayerTwoAttack: {
            this.attack(rightFighter, leftFighter);
            break;
        }
        case controls.PlayerTwoBlock: {
            this.blockOn(rightFighter);
            break;
        }
        case controls.PlayerTwoCriticalHitCombination[0]: {
            this.criticalHitKeyDownHandler (event.code, rightFighter, leftFighter);
            break;
        }
        case controls.PlayerTwoCriticalHitCombination[1]: {
            this.criticalHitKeyDownHandler (event.code, rightFighter, leftFighter);
            break;
        }
        case controls.PlayerTwoCriticalHitCombination[2]: {
            this.criticalHitKeyDownHandler (event.code, rightFighter, leftFighter);
            break;
        }
    }
  }

  keyUpHandler(event, leftFighter, rightFighter) {
    switch(event.code){
        case controls.PlayerOneBlock: {
            this.blockOff(leftFighter);
            break;
        }
        case controls.PlayerOneCriticalHitCombination[0]: {
            this.criticalHitKeyUpHandler (event.code, leftFighter);
            break;
        }
        case controls.PlayerOneCriticalHitCombination[1]: {
            this.criticalHitKeyUpHandler (event.code, leftFighter);
            break;
        }
        case controls.PlayerOneCriticalHitCombination[2]: {
            this.criticalHitKeyUpHandler (event.code, leftFighter);
            break;
        }
        case controls.PlayerTwoBlock: {
            this.blockOff(rightFighter);
            break;
        }
        case controls.PlayerTwoCriticalHitCombination[0]: {
            this.criticalHitKeyUpHandler (event.code, rightFighter);
            break;
        }
        case controls.PlayerTwoCriticalHitCombination[1]: {
            this.criticalHitKeyUpHandler (event.code, rightFighter);
            break;
        }
        case controls.PlayerTwoCriticalHitCombination[2]: {
            this.criticalHitKeyUpHandler (event.code, rightFighter);
            break;
        }
    }
  }

  initFighter(givenFighter, position) {
    const fighter = {...givenFighter};
    fighter.block = false;
    fighter.currentHealth = givenFighter.health;
    fighter.canCriticalHit = true;
    fighter.position = position;
    if (position === 'left'){
        for (let key of controls.PlayerOneCriticalHitCombination) {
            fighter[key] = false;
        }
    }
    else if (position === 'right') {
        for (let key of controls.PlayerTwoCriticalHitCombination){
            fighter[key] = false;
        }
    }
    return fighter;
  }

  attack(attacker, defender) {
    const damage = attacker.block || defender.block ? 0 : this.getDamage(attacker, defender);
    defender.currentHealth -= damage;
    this.decreaseHealthBar(defender);
    let note = {};
    if (attacker.position === 'left'){
        note.fighter1Shot = damage;
        note.fighter2Shot = 0;
        note.fighter1Health = attacker.currentHealth;
        note.fighter2Health = defender.currentHealth;
    } else {
        note.fighter1Shot = 0;
        note.fighter2Shot = damage;
        note.fighter1Health = defender.currentHealth;
        note.fighter2Health = attacker.currentHealth;
    }
    const notes = [...this.state.log];
    notes.push(note);
    this.setState({
        log: notes
    });
  }

  blockOn(fighter) {
    fighter.block = true;
  }

  blockOff(fighter) {
    fighter.block = false;
  }


  decreaseHealthBar(fighter){
    if(fighter.currentHealth < 0) fighter.currentHealth = 0;
    const change = fighter.currentHealth * 100 / fighter.health;
    if (fighter.position === 'left') this.setState({width1: `${change}%`});
    else this.setState({width2: `${change}%`});
  }

  criticalHitKeyDownHandler (key, attacker, defender) {
    if (!attacker.canCriticalHit || attacker.block) return;
    attacker[key] = true;
    let isCriticalHit = true;
    const combination = attacker.position === 'left' ? controls.PlayerOneCriticalHitCombination : controls.PlayerTwoCriticalHitCombination;
    Object.keys(attacker).forEach((key) => {
        if(combination.includes(key) && attacker[key] === false) {
            isCriticalHit = false;
        }
    })
    if (isCriticalHit){
        attacker.canCriticalHit = false;
        attacker.needChange = true;
        defender.currentHealth -= attacker.power * 2;
        this.decreaseHealthBar(defender);
        setTimeout(()=>{
            attacker.canCriticalHit = true;
        }, CRITIKAL_HIT_INTERVAL)
    }
  }

  criticalHitKeyUpHandler(key, fighter) {
    fighter[key] = false;
  }

  render() {
    const { fighter1, fighter2, width1, width2 } = this.state;
    if(this.state.winner) {
        return (
            <div id="fight-wrapper">
                <div id="arena___root">
                    <div id="arena___fight-status">                
                        <HealthBar fighterName={fighter1.name} width={width1}/>
                        <HealthBar fighterName={fighter2.name} width={width2}/>
                    </div>
                    <div id="arena___battlefield">                
                        <FighterView fighterName={fighter1.name} position="left"/>
                        <FighterView fighterName={fighter2.name} position="right"/>
                    </div>
                </div>
                <div id="modal-layer">
                    <div class="modal-root">
                        <div class="modal-header">
                            <span>And the winner becomes...</span>
                            <div class="close-btn" onClick={this.onFightEnd}>×</div>
                        </div>
                        <div class="modal-body">
                            <span class="modal___winner-name">{this.state.winner.name}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    if(this.state.isGameStarted) {
        return (
            <div id="arena___root">
                <div id="arena___fight-status">               
                    <HealthBar fighterName={fighter1.name} width={width1}/>
                    <HealthBar fighterName={fighter2.name} width={width2}/>
                </div>
                <div id="arena___battlefield">                
                    <FighterView fighterName={fighter1.name} position="left"/>
                    <FighterView fighterName={fighter2.name} position="right"/>
                </div>
            </div>
        );
    }
    return (
        <div id="wrapper">
            <NewFighter onCreated={this.onCreate} />
            <div id="figh-wrapper">
            <Fighter
                selectedFighter={fighter1}
                onFighterSelect={this.onFighter1Select}
                fightersList={this.getFighter1List() || []}
            />
            <div className="btn-wrapper">
                <Button
                onClick={this.onFightStart}
                variant="contained"
                color="primary"
                >
                Start Fight
                </Button>
            </div>
            <Fighter
                selectedFighter={fighter2}
                onFighterSelect={this.onFighter2Select}
                fightersList={this.getFighter2List() || []}
            />
            </div>
        </div>
    );
    
  }
}

export default Fight;
