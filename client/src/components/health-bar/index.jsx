import React from "react";
import './health-bar.css';

export default function HealthBar({fighterName, width}) {
    return (
        <div class="arena___fighter-indicator">
            <span class="arena___fighter-name">{fighterName}</span>
            <div class="arena___health-indicator">
                <div class="arena___health-bar" style={{width}}>
                </div>
            </div>
        </div>
    );
};