import React from "react";
import './fighter-view.css';

export default function FighterView({fighterName, position}) {
    const source = position === "left" ?
    "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif" :
    "https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"
    return (
        <div class={"arena___fighter arena___" + position + "-fighter"}>
            <img class="fighter-img" src={source} title={fighterName} alt={fighterName}/>
        </div>
    );
};