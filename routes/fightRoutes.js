const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/', (req, res, next) => {
  try {
    res.data = FightService.getFights();
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    res.data = FightService.getFight(req.params.id);
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', (req, res, next) => {
  try {
    const { fighter1, fighter2, log } = req.body;
    res.data = FightService.createFight({ fighter1: fighter1.id, fighter2: fighter2.id, log });
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;
