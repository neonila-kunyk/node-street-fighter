const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
  try {
    res.data = FighterService.getFighters();
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    res.data = FighterService.getFighter(req.params.id);
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  if (res.err) return next();
  try {
    res.data = FighterService.createFighter(req.body);
  } catch (err) {
    err.type = 'create';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  if (res.err) next();
  try {
    res.data = FighterService.updateFighter(req.params.id, req.body);
  } catch (err) {
    err.type = 'update';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    FighterService.deleteFighter(req.params.id);
  } catch (err) {
    err.type = 'delete';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;
