const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
  try {
    res.data = UserService.getUsers();
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    res.data = UserService.getUser(req.params.id);
  } catch (err) {
    err.type = 'not_found';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  if (res.err) return next();
  try {
    res.data = UserService.createUser(req.body);
  } catch (err) {
    err.type = 'create';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  if (res.err) next();
  try {
    res.data = UserService.updateUser(req.params.id, req.body);
  } catch (err) {
    err.type = 'update';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    UserService.deleteUser(req.params.id);
  } catch (err) {
    err.type = 'delete';
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;
