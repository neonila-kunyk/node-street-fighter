const isIdInRequest = (body) => Object.keys(body).includes('id');

const isAllFieldsInRequest = (body, entity) => {
  let flag = true;
  const acceptedFields = Object.keys(entity).filter((elem) => elem !== 'id' && elem !== 'health');
  acceptedFields.forEach((field) => {
    if (!(Object.keys(body).includes(field))) {
      flag = false;
      return false;
    }
  });
  return flag;
};

const isAtLeastOneFieldInRequest = (body, entity) => {
  let flag = false;
  const acceptedFields = Object.keys(entity).filter((elem) => elem !== 'id');
  acceptedFields.forEach((field) => {
    if (Object.keys(body).includes(field)) {
      flag = true;
      return true;
    }
  });
  return flag;
};

const areOnlyModelFields = (body, entity) => {
  let flag = true;
  const acceptedFields = Object.keys(entity).filter((elem) => elem !== 'id');
  Object.keys(body).forEach((key) => {
    if (!(acceptedFields.includes(key))) {
      flag = false;
      return false;
    }
  });
  return flag;
};

module.exports = {
  isIdInRequest,
  isAllFieldsInRequest,
  isAtLeastOneFieldInRequest,
  areOnlyModelFields
};
