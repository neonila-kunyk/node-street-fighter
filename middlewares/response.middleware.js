const getErrorCode = (type) => {
  if (type === 'validate'
    || type === 'create'
    || type === 'delete'
    || type === 'update') { return 400; }
  if (type === 'not_found') { return 404; }
  return 500;
};

const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  if (res.err) {
    return res.status(getErrorCode(res.err.type)).json({ error: true, message: res.err.message });
  }
  if (res.data) {
    return res.status(200).json(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
