const { user } = require('../models/user');
const modelValid = require('./model.validation.middleware');

const isValidName = (username) => {
  const regex = /^[a-zA-Z-]+$/;
  return !!username && regex.test(username);
};

const isValidEmail = (email) => {
  const regex = /^[a-zA-Z0-9.-_]+@gmail\.com$/;
  return !!email && regex.test(email.toLowerCase());
};

const isValidPhone = (phone) => {
  const regex = /^(\+380)\d{9}$/;
  return !!phone && regex.test(phone);
};

const isValidPassword = (password) => {
  const regex = /^[0-9a-zA-Z!@#$%^&*]{3,}$/;
  return !!password && regex.test(password);
};

const validate = (req, type) => {
  if (type === 'create') if (!modelValid.isAllFieldsInRequest(req.body, user)) throw Error('Please enter all necessary fields');
  if (type === 'update') if (!modelValid.isAtLeastOneFieldInRequest(req.body, user)) throw Error('Please enter all necessary fields');
  if (!modelValid.areOnlyModelFields(req.body, user)) throw Error('Please enter only allowed fields');
  if (modelValid.isIdInRequest(req.body, user)) throw Error("Please don't declare id");
  if (!isValidName(req.body.firstName)) throw Error('Please enter valid first name');
  if (!isValidName(req.body.lastName)) throw Error('Please enter valid last name');
  if (!isValidEmail(req.body.email)) throw Error('Please enter a valid email (only @gmail.com is allowed)');
  if (!isValidPhone(req.body.phoneNumber)) throw Error('Please enter a valid phone number (+380xxxxxxxxx)');
  if (!isValidPassword(req.body.password)) throw Error('Please enter valid password (3 or more characters)');
};

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  try {
    validate(req, 'create');
  } catch (err) {
    err.type = 'validate';
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  try {
    validate(req, 'update');
  } catch (err) {
    err.type = 'validate';
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
