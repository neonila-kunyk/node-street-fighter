const { fighter } = require('../models/fighter');
const modelValid = require('./model.validation.middleware');

const isValidName = (fightername) => {
  const regex = /^[a-zA-Z]+$/;
  return !!fightername && regex.test(fightername);
};

const isValidPower = (power) => !!power && power >= 1 && power <= 100;

const isValidDefense = (defense) => !!defense && defense >= 1 && defense <= 10;

const isValidHealth = (health) => !!health && health >= 80 && health <= 20;

const validate = (req, type) => {
  if (type === 'create') if (!modelValid.isAllFieldsInRequest(req.body, fighter)) throw Error('Please enter all necessary fields');
  if (type === 'update') if (!modelValid.isAtLeastOneFieldInRequest(req.body, fighter)) throw Error('Please enter all necessary fields');
  if (!modelValid.areOnlyModelFields(req.body, fighter)) throw Error('Please enter only allowed fields');
  if (modelValid.isIdInRequest(req.body, fighter)) throw Error("Please don't declare id");
  if (!isValidName(req.body.name)) throw Error('Please enter valid name');
  if (!isValidPower(req.body.power)) throw Error('Please enter a valid power (number from 1 to 100)');
  if (!isValidDefense(req.body.defense)) throw Error('Please enter a valid defense (number from 1 to 10)');
  if (req.body.health && !isValidHealth(req.body.health)) throw Error('Please enter a valid health (number from 80 to 120)');
};

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    validate(req, 'create');
  } catch (err) {
    err.type = 'validate';
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  try {
    validate(req, 'update');
  } catch (err) {
    err.type = 'validate';
    res.err = err;
  } finally {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
