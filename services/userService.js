const { UserRepository } = require('../repositories/userRepository');

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getUsers() {
    const users = UserRepository.getAll();
    if (!users) {
      throw Error('No user has been created at the moment');
    }
    return users;
  }

  getUser(id) {
    const user = this.search({ id });
    if (!user) {
      throw Error('User with this ID does not exist');
    }
    return user;
  }

  checkExistingByEmail(email) {
    const users = this.getUsers();
    users.forEach((user) => {
      if (user.email.toLowerCase() === email.toLowerCase()) throw Error('User with such email already exists');
    });
  }

  checkExistingByPhone(phone) {
    const users = this.getUsers();
    users.forEach((user) => {
      if (user.phoneNumber.toLowerCase() === phone.toLowerCase()) throw Error('User with such phone number already exists');
    });
  }

  createUser(data) {
    const { email, phoneNumber } = data;
    this.checkExistingByEmail(email);
    this.checkExistingByPhone(phoneNumber);

    const newUser = UserRepository.create(data);
    if (!newUser) {
      return null;
    }
    return newUser;
  }

  updateUser(id, data) {
    this.getUser(id);
    const updatedUser = UserRepository.update(id, data);
    if (!updatedUser) {
      return null;
    }
    return updatedUser;
  }

  deleteUser(id) {
    this.getUser(id);
    const deletedUser = UserRepository.delete(id);
    if (!deletedUser) {
      return null;
    }
    return deletedUser;
  }
}

module.exports = new UserService();
