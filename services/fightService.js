const { FightRepository } = require('../repositories/fightRepository');

class FightsService {
  // OPTIONAL TODO: Implement methods to work with fights
  getFights() {
    const fights = FightRepository.getAll();
    if (!fights) {
      throw Error('Fights history is empty now');
    }
    return fights;
  }

  createFight(data) {
    const newFight = FightRepository.create(data);
    if (!newFight) {
      throw Error('Could not create a new fight');
    }
    return newFight;
  }

  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FightsService();
