const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getFighters() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      throw Error('No fighter has been created at the moment');
    }
    return fighters;
  }

  getFighter(id) {
    const fighter = this.search({ id });
    if (!fighter) {
      throw Error('Fighter with this ID does not exist');
    }
    return fighter;
  }

  checkExistingByName(name) {
    const fighters = this.getFighters();
    fighters.forEach((fighter) => {
      if (fighter.name.toLowerCase() === name.toLowerCase()) throw Error('Fighter with such name already exists');
    });
  }

  createFighter(data) {
    this.checkExistingByName(data.name);
    if (!data.health) data.health = 100;

    const newFighter = FighterRepository.create(data);
    if (!newFighter) {
      return null;
    }
    return newFighter;
  }

  updateFighter(id, data) {
    this.getFighter(id);
    const updatedFighter = FighterRepository.update(id, data);
    if (!updatedFighter) {
      return null;
    }
    return updatedFighter;
  }

  deleteFighter(id) {
    this.getFighter(id);
    const deletedFighter = FighterRepository.delete(id);
    if (!deletedFighter) {
      return null;
    }
    return deletedFighter;
  }
}

module.exports = new FighterService();
